#!/bin/bash
#Se le pide al usuario el codigo alfanumerico de la proteína a graficar, este dato ingresado puede ser tanto en minusculas como mayusculas
#ya que se le modifica para que sea mayusculas y así ser comparado en la base de datos, la cual tambien fue modificada para considerar solo 
#los caracteres alfanumericos de las filas que hayan sido "Protein"
comparador (){
	cat bd-pdb.txt |grep '"Protein"$'| awk -F"," '{print substr($1,2,4)} ' > alfanum.txt
	database=`grep -io "$1" ./alfanum.txt`
	name=`echo $1 |awk '{print toupper($0)}'`	
}
descargador (){
		wget https://files.rcsb.org/download/$1.pdb
		echo "Descargada"
}
procesador (){
	
	cat $1.pdb |grep '^ATOM' | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7 " " $8 " " $9}' > ATOM.txt
	cat $1.pdb |grep '^HETATM' | awk '{if ($4 != "HOH") print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7 " " $8 " " $9}' > HETATM.txt
	awk -f ligandos.awk HETATM.txt > HETATOM.txt
}
graficador (){
	echo "Ahora su proteína procederá a ser graficada"
	
}

existe=0
wget https://icb.utalca.cl/~fduran/bd-pdb.txt
while :; do
echo "------------Bienvenido al Visualizador de ligandos-------------"
echo "Ingrese la proteina a analizar, esta debe ser reconocida por la base de datos"
read name
comparador $name
#aqui comparamos lo ingresado con la database
if [ "${name}" = "${database}" ]; then
	resant=0
	existe=1
	echo "Su proteína ha sido aceptada segun la base de datos."
	echo "Ingrese el radio (A) en el que se va a evaluar:"
	read radio
	descargador $name
	procesador $name
	echo "Ahora su proteína procederá a ser graficada"
	while read ligando; do
		namegraf=`echo -e "$ligando" | awk '{print $4$6 "CAD" $5 }'`
		echo "digraph G {" > $namegraf.dot
		echo "rankdir=LR;" >> $namegraf.dot
		echo "node [shape=box];" >> $namegraf.dot
		echo -e "$ligando" | awk '{print $4";"}' >> $namegraf.dot
		echo "node [ shape = circle ]" >> $namegraf.dot
		lig=`echo -e "$ligando" | awk '{print $4}'`
		xcg=`echo -e "$ligando" | awk '{print $7}'`
		ycg=`echo -e "$ligando" | awk '{print $8}'`
		zcg=`echo -e "$ligando" | awk '{print $9}'`
		while read atomo; do
			xatom=`echo -e "$atomo" | awk '{print $7}'`
			yatom=`echo -e "$atomo" | awk '{print $8}'`
			zatom=`echo -e "$atomo" | awk '{print $9}'`
			xdef=$(bc <<< "($xatom - $xcg)^2")
			ydef=$(bc <<< "($yatom - $ycg)^2")
			zdef=$(bc <<< "($zatom - $zcg)^2")
			sumadef=$(bc <<< "$xdef+$ydef+$zdef")
			dist=$(bc <<< "scale=0; sqrt($sumadef)")
			echo $dist
			dist=`echo "scale=0; ($dist/1)+1" | bc`
			echo $xdef	
			echo $ydef
			echo $zdef
			echo $dist
			echo "era."
			if [[ ${dist} -lt ${radio} ]]; then
				res=`echo -e "$atomo" | awk '{print $4%5}'` #aqui se obtiene el res al que pertenece el atomo que se está midiendo distancia
				echo "rango de los cercanos"
				if [[ "${res}" != "${resant}" ]]; then
				echo "son de distinto res y caAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAad"
				meteres=`echo -e "$atomo" | awk '{print toupper($4)}'`
				
				echo $meteres " -> "$lig ";">> $namegraf.dot
				echo " metido"
				resant=`echo -e "$atomo" | awk '{print $4$5}'`
				fi;
			fi;
		done < ATOM.txt
		echo "}" >> $namegraf.dot
	done < HETATOM.txt
	dot - Tpng -o $namegraf.png $namegraf.dot
	# rm ATOM.txt
	# rm HETATOM.txt
	# rm HETATM.txt
	# rm alfanum.txt
	echo "Muchas gracias por usar el software, si nuestra ayuda le ha servido no dude en transferir la cantidad que usted estime conveniente a la cuenta rut 20007855 banco estado."
	break
else
	#al no encontrar coincidencia, se le pide al usuario que lo vuelva a intantar
	echo "No se ha encontrado coincidencias con la proteína ingresada en la base de datos, intente nuevamente con una proteína que se encuentre en la base de datos."	
	existe=0
fi
done
